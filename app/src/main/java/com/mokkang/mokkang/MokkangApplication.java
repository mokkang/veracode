package com.mokkang.mokkang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MokkangApplication {

	public static void main(String[] args) {
		SpringApplication.run(MokkangApplication.class, args);
	}

}
